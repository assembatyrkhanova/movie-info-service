package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController

@RequestMapping("/movie/info")

public class MovieController {





    @GetMapping("/{userId}")

    public UserMovie getMoviesByUserId(

            @PathVariable("userId") String userId) {



        List<Movie> userMovieList =  Arrays.asList(

                new Movie("1", "Movie 1", "Horror", "Author 1", "Cool Movie"),

                new Movie("2", "Movie 2", "Romantic", "Author 2", "Cool Movie"),

                new Movie("3", "Movie 3", "Fantasy", "Author 3", "Cool Movie"),

                new  Movie("4", "Movie 4", "Horror", "Author 4", "Cool Movie"));



        UserMovie userMovie = new UserMovie(userMovieList);



        return userMovie;

    }

}
